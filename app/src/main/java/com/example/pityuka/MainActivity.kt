package com.example.pityuka

import android.content.Context
import android.media.MediaPlayer
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.pityuka.ui.theme.PityukaTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContent {
            PityukaTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Felsorolas()
                }
            }
        }
    }
}
private fun playSound(soundResId: Int, context: Context) {
    currentMediaPlayer?.stop()  // Stop any previously playing sound
    currentMediaPlayer?.release() // Release resources
    currentMediaPlayer = null

    currentMediaPlayer = MediaPlayer.create(context, soundResId)
    currentMediaPlayer?.start()

    currentMediaPlayer?.setOnErrorListener { _, what, extra ->
        Log.e("SoundPlayer", "Error playing sound: $what, $extra")
        true  // True indicates we handled the error
    }
}
var currentMediaPlayer: MediaPlayer? = null

@Composable
fun Felsorolas() {

    val context = LocalContext.current

    Text(
        text = "MC ISTI SOUNDBOARD \uD83C\uDF4D",
        modifier = Modifier.padding(top = 64.dp), // Adjust the padding value as needed
        textAlign = TextAlign.Center
    )
    LazyVerticalGrid(
        columns = GridCells.Adaptive(100.dp),
        modifier = Modifier.padding(top = 140.dp),
        content = {

            items(1) {
                Image(
                    painterResource(id = R.drawable.mc2),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.kordonblu, context)
                        }
                )


            }
            items(1) {
                Image(
                    painterResource(id = R.drawable.mc3),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.viii, context)
                        })
            }

            items(1) {
                Image(
                    painterResource(id = R.drawable.mc4),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.coca, context)
                        })
            }

            items(1) {
                Image(
                    painterResource(id = R.drawable.mc5),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.dudal, context)
                        })
            }

            items(1) {
                Image(
                    painterResource(id = R.drawable.mc6),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.durranas, context)
                        })
            }

            items(1) {
                Image(
                    painterResource(id = R.drawable.mc7),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.hokizok, context)
                        })
            }

            items(1) {
                Image(
                    painterResource(id = R.drawable.mc8),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.irjad, context)
                        })
            }

            items(1) {
                Image(
                    painterResource(id = R.drawable.mc9),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.karomkodas, context)
                        })
            }

            items(1) {
                Image(
                    painterResource(id = R.drawable.mc10),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.koki, context)
                        })
            }

            items(1) {
                Image(
                    painterResource(id = R.drawable.mc11),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.amiskolcielmebeteg, context)
                        })
            }

            items(1) {
                Image(
                    painterResource(id = R.drawable.mc12),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.milliardos, context)
                        })
            }

            items(1) {
                Image(
                    painterResource(id = R.drawable.mc13),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.nyiregyhaza, context)
                        })
            }

            items(1) {
                Image(
                    painterResource(id = R.drawable.mc14),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.pizza, context)
                        })
            }

            items(1) {
                Image(
                    painterResource(id = R.drawable.mc15),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.rage, context)
                        })
            }

            items(1) {
                Image(
                    painterResource(id = R.drawable.mc16),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.bolond, context)
                        })
            }


            items(1) {
                Image(
                    painterResource(id = R.drawable.mc22),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.aazaria, context)
                        })
            }


            items(1) {
                Image(
                    painterResource(id = R.drawable.mc23),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.abolond, context)
                        })
            }


            items(1) {
                Image(
                    painterResource(id = R.drawable.mc24),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.aburger, context)
                        })
            }


            items(1) {
                Image(
                    painterResource(id = R.drawable.mc25),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.aborond, context)
                        })
            }


            items(1) {
                Image(
                    painterResource(id = R.drawable.mc26),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.adancso, context)
                        })
            }


            items(1) {
                Image(
                    painterResource(id = R.drawable.mc27),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.afenyegetes, context)
                        })
            }


            items(1) {
                Image(
                    painterResource(id = R.drawable.mc28),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.afurdik, context)
                        })
            }


            items(1) {
                Image(
                    painterResource(id = R.drawable.mc29),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.afgyuras, context)
                        })
            }


            items(1) {
                Image(
                    painterResource(id = R.drawable.mc30),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.akujbasz, context)
                        })
            }


            items(1) {
                Image(
                    painterResource(id = R.drawable.mc31),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.apofozva, context)
                        })
            }


            items(1) {
                Image(
                    painterResource(id = R.drawable.mc32),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.aspanyol, context)
                        })
            }
            items(1) {
                Image(
                    painterResource(id = R.drawable.mc33),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.ahugyos, context)
                        })
            }

            items(1) {
                Image(
                    painterResource(id = R.drawable.mc39),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.aaa1, context)
                        })
            }

            items(1) {
                Image(
                    painterResource(id = R.drawable.mc40),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.aaa2, context)
                        })
            }

            items(1) {
                Image(
                    painterResource(id = R.drawable.mc41),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.aaa3, context)
                        })
            }

            items(1) {
                Image(
                    painterResource(id = R.drawable.mc42),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.aaa4, context)
                        })
            }

            items(1) {
                Image(
                    painterResource(id = R.drawable.mc43),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.aaa5, context)
                        })
            }

            items(1) {
                Image(
                    painterResource(id = R.drawable.mc44),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.aaa6, context)
                        })
            }

            items(1) {
                Image(
                    painterResource(id = R.drawable.mc45),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.aaa7, context)
                        })
            }

            items(1) {
                Image(
                    painterResource(id = R.drawable.mc37),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.aaa8, context)
                        })
            }

            items(1) {
                Image(
                    painterResource(id = R.drawable.mc38),
                    contentDescription = "pityu",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(width = 220.dp, height = 110.dp)
                        .clickable {
                            playSound(R.raw.aaa9, context)
                        })
            }

        }
    )
    Box(){
        Text(
            text = "Ha tetszik az app, támogass algoval! Az Algorand tárcám QR kódja:",
            textAlign = TextAlign.Center,
            color = Color.Magenta,
            modifier = Modifier
                .padding(top = 655.dp)
        )
    }
    Box(
        modifier = Modifier
            .fillMaxSize(),
        contentAlignment = Alignment.BottomCenter,
        )
    {
        Image(
            painterResource(id = R.drawable.algoqr), contentDescription = null,
            modifier = Modifier
                .padding(20.dp)
                .size(width = 160.dp, height = 80.dp)
        )
    }
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    PityukaTheme {
        Felsorolas()
    }
}